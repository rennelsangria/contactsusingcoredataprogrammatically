//
//  DisplayPersonViewController.m
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/12/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "DisplayPersonViewController.h"

@interface DisplayPersonViewController ()

@end

@implementation DisplayPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    [self.navigationItem setTitle:@"Contact Info"];
    
//create EDIT button on the nav bar
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Edit"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(editContact:)];
    [self.navigationItem setRightBarButtonItem: editButton];
    
    
    UILabel* contactName = [[UILabel alloc]initWithFrame:CGRectMake(10, 70, self.view.frame.size.width-20, 30)];
    contactName.text = @"Name:";
    contactName.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contactName];
    
    //create the label to display the name of the contact
    nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(90, 70, self.view.frame.size.width-90, 30)];
    nameLbl.text = [_obj valueForKey:@"name"];
    nameLbl.textColor = [UIColor blueColor];
    [self.view addSubview:nameLbl];
    
    UILabel* contactPhoneNum = [[UILabel alloc]initWithFrame:CGRectMake(10, 110, self.view.frame.size.width-20, 30)];
    contactPhoneNum.text = @"Phone #:";
    contactPhoneNum.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contactPhoneNum];
    
    //create the label to display the phone number of the contact
    phoneLbl = [[UILabel alloc] initWithFrame:CGRectMake(90, 110, self.view.frame.size.width-90, 30)];
    phoneLbl.text = [_obj valueForKey:@"phone_number"];
    phoneLbl.textColor = [UIColor blueColor];
    [self.view addSubview:phoneLbl];
    
 
    //create the Send Text Messaging button
    UIButton* smsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage* txtImg = [UIImage imageNamed:@"TextIcon.png"];
    [smsButton setBackgroundImage:txtImg forState:UIControlStateNormal];
    smsButton.frame = CGRectMake(90, 150, 50, 50);
    //smsButton.backgroundColor = [UIColor blueColor];
    //[smsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[[smsButton layer] setCornerRadius:8.0f];
    //[[smsButton layer] setBorderColor: [UIColor blueColor].CGColor];
    [smsButton addTarget:self action:@selector(sendText:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:smsButton];
    
    //create the Call button
    UIButton* callButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage* callImg = [UIImage imageNamed:@"PhoneIcon.png"];
    [callButton setBackgroundImage:callImg forState:UIControlStateNormal];
    //[callButton setTitle:@"Call" forState:UIControlStateNormal];
    callButton.frame = CGRectMake(230, 150, 55, 55);
    //callButton.backgroundColor = [UIColor greenColor];
    //[[smsButton layer] setCornerRadius:8.0f];
    //[[smsButton layer] setBorderColor: [UIColor blueColor].CGColor];
    [callButton addTarget:self action:@selector(makeCall:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:callButton];
    
 //create the email section
    
    emailString= [[NSString alloc] init];
    emailString = [_obj valueForKey:@"email"];
    
    if([emailString length] == 0 || emailString == nil){
        contactEmail = [[UILabel alloc]initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 50)];
        contactEmail.text = @"Email address is unknown";
        contactEmail.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:contactEmail];
        
    }else{
        
        contactEmail = [[UILabel alloc]initWithFrame:CGRectMake(10, 210, self.view.frame.size.width-20, 50)];
        contactEmail.text = @"Email:";
        contactEmail.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:contactEmail];
        
        
        
        //set the email address in a button
        
        //set the size of the button according to email address text length
        CGSize stringsize = [emailString sizeWithFont:[UIFont systemFontOfSize:14.0f]];
        emailButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [emailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [emailButton setTitle:emailString forState:UIControlStateNormal];
        emailButton.backgroundColor = [UIColor blueColor];
        emailButton.frame = CGRectMake(90, 220, stringsize.width + 20, 30);
        [[emailButton layer] setCornerRadius:8.0f];
        [[emailButton layer] setBorderWidth:2.0f];
        [[emailButton layer] setBorderColor:[UIColor darkGrayColor].CGColor];
        [emailButton addTarget:self action:@selector(sendEmail:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:emailButton];
    }
    
 //create the address section
    UILabel* contactAddress = [[UILabel alloc]initWithFrame:CGRectMake(10, 270, self.view.frame.size.width-20, 130)];
    contactAddress.text = @"Address:";
    contactAddress.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contactAddress];
    
    
    //get the address1 and address2 to concatenate
    address1_Lbl= [[NSString alloc] init];
    address1_Lbl = [_obj valueForKey:@"address_1"];
    
    address2_Lbl= [[NSString alloc] init];
    address2_Lbl = [_obj valueForKey:@"address_2"];
    
    bldgAddress = [[NSString alloc]init];
    if([address2_Lbl length] == 0 || address2_Lbl == nil){
        bldgAddress = address1_Lbl;
    }else{
        bldgAddress = [address1_Lbl stringByAppendingString:@", "];
        bldgAddress = [bldgAddress stringByAppendingString:address2_Lbl];
    }
    
    //display the address
    addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(90, 280, self.view.frame.size.width-90, 30)];
    addressLbl.text = bldgAddress;
    addressLbl.textColor = [UIColor blueColor];
    [self.view addSubview:addressLbl];
    
    //get the city, state and zip to concatenate
    cityLbl= [[NSString alloc] init];
    cityLbl = [_obj valueForKey:@"city"];
 
    stateLbl= [[NSString alloc] init];
    stateLbl = [_obj valueForKey:@"state"];
    
    zipLbl= [[NSString alloc] init];
    zipLbl = [_obj valueForKey:@"zip"];
    
    
    cityAddress = [cityLbl stringByAppendingString:@", "];
    cityAddress = [cityAddress stringByAppendingString:stateLbl];
    cityAddress = [cityAddress stringByAppendingString:@" "];
    cityAddress = [cityAddress stringByAppendingString:zipLbl];
    
    cityStateZip = [[UILabel alloc] initWithFrame:CGRectMake(90, 310, self.view.frame.size.width-90, 30)];
    cityStateZip.text = cityAddress;
    cityStateZip.textColor = [UIColor blueColor];
    [self.view addSubview:cityStateZip];
    
    
    //add the map icon button at the right of the address
    UIButton* mapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage* mapImg = [UIImage imageNamed:@"MapsIcon.png"];
    [mapButton setBackgroundImage:mapImg forState:UIControlStateNormal];
    mapButton.frame = CGRectMake(90, 340, 50, 50);
    //callButton.backgroundColor = [UIColor greenColor];
    //[[smsButton layer] setCornerRadius:8.0f];
    //[[smsButton layer] setBorderColor: [UIColor blueColor].CGColor];
    [mapButton addTarget:self action:@selector(showMap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mapButton];
    
    
    //add the image of the person
    imgview = [[UIImageView alloc]
                            initWithFrame:CGRectMake((self.view.frame.size.width/2)-100 , 420, 200, 200)];
    //if the image is empty; use a placeholder image
    if(_obj == nil  || [_obj valueForKey:@"image"] == nil){
        
        [imgview setImage:[UIImage imageNamed:@"personIcon.png"]];
        [self.view addSubview:imgview];
    }
    else{
        [imgview setImage:[UIImage imageWithData:[_obj valueForKey:@"image"]]];
        [self.view addSubview:imgview];
    } 
    
    //[self.view setNeedsDisplay];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//send text message function
-(void) sendText:(id)sender{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = @"SMS message here";
            NSString *cleanedString = [[[object valueForKey:@"phone_number"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
            
            controller.recipients = [NSArray arrayWithObjects:cleanedString, nil];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:^{}];
        }
}

//make a phone call function
-(void) makeCall:(id)sender{
    //Call someone
        NSLog(@"%@", object);
        NSString *cleanedString = [[[object valueForKey:@"phone_number"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
}


//send email
-(void) sendEmail:(id)sender{
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"My Subject"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        [controller setToRecipients:@[[object valueForKey:@"email"]]];
        [self presentViewController:controller animated:YES completion:^{
    
        }];
}

-(void) showMap:(id)sender{
    
    mapViewController* mapvc = [mapViewController new];
    mapvc.obj = _obj;
    
    NSLog(@"%@",mapvc.obj);
    [self.navigationController pushViewController:mapvc animated:YES];
 
}


-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:controller completion:^{
        
    }];
  
}

-(void)editContact:(id)sender{
    editViewController* editvc = [editViewController new];
    editvc.obj = _obj;
    
    NSLog(@"%@",editvc.obj);
    [self.navigationController pushViewController:editvc animated:YES];
    
}

- (void)viewWillAppear:(BOOL)animated{
    nameLbl.text = [_obj valueForKey:@"name"];
    phoneLbl.text = [_obj valueForKey:@"phone_number"];
    
    emailString = [_obj valueForKey:@"email"];
    
    if([emailString length] == 0 || emailString == nil){
                contactEmail.text = @"Email address is unknown";
        emailButton.frame = CGRectMake(0, 0, 0, 0);
    }
    
    if (emailString != nil){
        contactEmail.text = @"Email:";
        CGSize stringsize = [emailString sizeWithFont:[UIFont systemFontOfSize:14.0f]];
        emailButton.frame = CGRectMake(90, 220, stringsize.width + 20, 30);
        [emailButton setTitle:emailString forState:UIControlStateNormal];
    }
    
    address1_Lbl = [_obj valueForKey:@"address_1"];
    address2_Lbl = [_obj valueForKey:@"address_2"];
    bldgAddress = [[NSString alloc]init];
    if([address2_Lbl length] == 0 || address2_Lbl == nil){
        bldgAddress = address1_Lbl;
    }else{
        bldgAddress = [address1_Lbl stringByAppendingString:@", "];
        bldgAddress = [bldgAddress stringByAppendingString:address2_Lbl];
    }
    
    addressLbl.text = bldgAddress;
    
    cityLbl = [_obj valueForKey:@"city"];
    stateLbl = [_obj valueForKey:@"state"];
    zipLbl = [_obj valueForKey:@"zip"];
    
    cityAddress = [cityLbl stringByAppendingString:@", "];
    cityAddress = [cityAddress stringByAppendingString:stateLbl];
    cityAddress = [cityAddress stringByAppendingString:@" "];
    cityAddress = [cityAddress stringByAppendingString:zipLbl];
    cityStateZip.text = cityAddress;
    
    if(_obj == nil  || [_obj valueForKey:@"image"] == nil){
        
        imgview.image =[UIImage imageNamed:@"personIcon.png"];
    }else{
        imgview.image = [UIImage imageWithData:[_obj valueForKey:@"image"]];
    }

}


@end
