//
//  ViewController.m
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/10/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController
@synthesize myTableView;
//@synthesize editViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Contacts";
    
    //create a submit button in the navigation bar
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                  target:self
                                  action:@selector(addContact:)];
    
    
//Getting our app delegate
    ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [ad managedObjectContext];
// deleting person object
    //[context deleteObject:person];
    
//Creating a new Person object to the core data
//    NSManagedObject *person = [NSEntityDescription
//                               insertNewObjectForEntityForName:@"Person"
//                               inManagedObjectContext:context];
//    [person setValue:@"John Doe" forKey:@"name"];
//    [person setValue:@"1900 N 2225W" forKey:@"address_1"];
//    [person setValue:@"Apt 100" forKey:@"address_2"];
//    [person setValue:@"Clinton" forKey:@"city"];
//    [person setValue:@"UT" forKey:@"state"];
//    [person setValue:@"84015" forKey:@"zip"];
//    [person setValue:@"johndoe@mail.weber.edu" forKey:@"email"];
//    [person setValue:@"702-280-3646" forKey:@"phone_number"];
//    
    //Saving person object
    NSError *error;
    BOOL saveSucceeded = [context save:&error];
    if (!saveSucceeded) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
//Fetching our person objects
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
//Looping through and diplaying the persons name
//    for (NSManagedObject *info in fetchedObjects) {
//        NSLog(@"Name: %@", [info valueForKey:@"name"]);
//        object = info;
        //Editing an object
        //        [info setValue:@"my name" forKey:@"name"];
        //
        //        if (![context save:&error]) {
        //            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        //        }
    //}
    

    
    //create the table view
    myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self.view addSubview:myTableView];
}//end of viewDidLoad


//notify the receiver that the fetched results controller has completed processing
//of one or more changes due to an add
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.myTableView endUpdates];
}

-(void)viewWillAppear:(BOOL)animated{
    
    //reload the object
    NSError *error;
    NSManagedObjectContext *context = [ad managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Person" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    [myTableView reloadData];
    
    [super viewDidAppear:animated];
}

//methods to display the table view
- (NSInteger)tableView:myTableView numberOfRowsInSection:(NSInteger)section{
    //test to display the size of the Person table
    NSUInteger size = [fetchedObjects count];
    NSLog(@"count of contact list: %lu", (unsigned long)size);
    return [fetchedObjects count];
}

//method that lists the names of the contact in the tableview
- (UITableViewCell *)tableView:myTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell=[myTableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text=[[fetchedObjects objectAtIndex:indexPath.row]valueForKey:@"name"];
    return cell;
}

//method when the table view row is touched
-(void)tableView:myTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [myTableView deselectRowAtIndexPath:indexPath animated:YES];
    DisplayPersonViewController* dpvc = [DisplayPersonViewController new];
    dpvc.obj = fetchedObjects[indexPath.row];
    [self.navigationController pushViewController:dpvc animated:YES];
}


-(void) addContact:(id)sender{
    //get reference to the button that reqeusted the action
    editViewController *add = [[editViewController alloc]init];
    
    //tell the navigation controller to go to editViewController page
    [self.navigationController pushViewController:add animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
