//
//  ViewController.h
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/10/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DisplayPersonViewController.h"
#import "editViewController.h"


@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>//<UIImagePickerControllerDelegate, UINavigationControllerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    AppDelegate* ad;
    NSManagedObject* object;
    NSArray* fetchedObjects;
}

@property (nonatomic, strong) UITableView *myTableView;


@end

