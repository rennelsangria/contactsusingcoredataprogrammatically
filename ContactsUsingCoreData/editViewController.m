//
//  editViewController.m
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/12/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "editViewController.h"

@interface editViewController ()

@end

@implementation editViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor purpleColor];
   
    [self.navigationItem setTitle:@"Create/Edit"];
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Save"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(saveContact:)];
    [self.navigationItem setRightBarButtonItem: saveButton];
    
//display the name textfield
    contactName = [[UITextField alloc]initWithFrame:CGRectMake(10, 100, self.view.frame.size.width-20, 40)];
    contactName.backgroundColor = [UIColor whiteColor];
    contactName.inputAccessoryView = [self inputView];
    contactName.placeholder = @"Enter Name";
    contactName.text = [_obj valueForKey:@"name"];
    contactName.keyboardType = UIKeyboardTypeDefault;
    contactName.returnKeyType = UIReturnKeyDone;
    contactName.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactName.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactName.delegate = self;
    [self.view addSubview:contactName];
    
//display the phone textfield
    contactPhone = [[UITextField alloc]initWithFrame:CGRectMake(10, 150, self.view.frame.size.width-20, 40)];
    contactPhone.backgroundColor = [UIColor whiteColor];
    contactPhone.inputAccessoryView = [self inputView];
    contactPhone.placeholder = @"Enter Phone Number";
    contactPhone.text = [_obj valueForKey:@"phone_number"];
    contactPhone.keyboardType = UIKeyboardTypeDefault;
    contactPhone.returnKeyType = UIReturnKeyDone;
    contactPhone.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactPhone.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactPhone.delegate = self;
    [self.view addSubview:contactPhone];
   
//display the email textfield
    contactEmail = [[UITextField alloc]initWithFrame:CGRectMake(10, 200, self.view.frame.size.width-20, 40)];
    contactEmail.backgroundColor = [UIColor whiteColor];
    contactEmail.inputAccessoryView = [self inputView];
    contactEmail.placeholder = @"Enter Email Address";
    contactEmail.text = [_obj valueForKey:@"email"];
    contactEmail.keyboardType = UIKeyboardTypeDefault;
    contactEmail.returnKeyType = UIReturnKeyDone;
    contactEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactEmail.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactEmail.delegate = self;
    [self.view addSubview:contactEmail];

//display the address1 textfield
    contactAddress1 = [[UITextField alloc]initWithFrame:CGRectMake(10, 250, self.view.frame.size.width-20, 40)];
    contactAddress1.backgroundColor = [UIColor whiteColor];
    contactAddress1.inputAccessoryView = [self inputView];
    contactAddress1.placeholder = @"Enter Street Address 1";
    contactAddress1.text = [_obj valueForKey:@"address_1"];
    contactAddress1.keyboardType = UIKeyboardTypeDefault;
    contactAddress1.returnKeyType = UIReturnKeyDone;
    contactAddress1.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactAddress1.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactAddress1.delegate = self;
    [self.view addSubview:contactAddress1];
    
//display the address2 textfield
    contactAddress2 = [[UITextField alloc]initWithFrame:CGRectMake(10, 300, self.view.frame.size.width-20, 40)];
    contactAddress2.backgroundColor = [UIColor whiteColor];
    contactAddress2.inputAccessoryView = [self inputView];
    contactAddress2.placeholder = @"Enter Street Address 2";
    contactAddress2.text = [_obj valueForKey:@"address_2"];
    contactAddress2.keyboardType = UIKeyboardTypeDefault;
    contactAddress2.returnKeyType = UIReturnKeyDone;
    contactAddress2.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactAddress2.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactAddress2.delegate = self;
    [self.view addSubview:contactAddress2];
    
//display the city textfield
    contactCity = [[UITextField alloc]initWithFrame:CGRectMake(10, 350, self.view.frame.size.width-20, 40)];
    contactCity.backgroundColor = [UIColor whiteColor];
    contactCity.inputAccessoryView = [self inputView];
    contactCity.placeholder = @"Enter City";
    contactCity.text = [_obj valueForKey:@"city"];
    contactCity.keyboardType = UIKeyboardTypeDefault;
    contactCity.returnKeyType = UIReturnKeyDone;
    contactCity.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactCity.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactCity.delegate = self;
    [self.view addSubview:contactCity];
   
//display the state textfield
    contactState = [[UITextField alloc]initWithFrame:CGRectMake(10, 400, self.view.frame.size.width-20, 40)];
    contactState.backgroundColor = [UIColor whiteColor];
    contactState.inputAccessoryView = [self inputView];
    contactState.placeholder = @"Enter State";
    contactState.text = [_obj valueForKey:@"state"];
    contactState.keyboardType = UIKeyboardTypeDefault;
    contactState.returnKeyType = UIReturnKeyDone;
    contactState.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactState.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactState.delegate = self;
    [self.view addSubview:contactState];

//display the zipcode textfield
    contactZip = [[UITextField alloc]initWithFrame:CGRectMake(10, 450, self.view.frame.size.width-20, 40)];
    contactZip.backgroundColor = [UIColor whiteColor];
    contactZip.inputAccessoryView = [self inputView];
    contactZip.placeholder = @"Enter Zipcode";
    contactZip.text = [_obj valueForKey:@"zip"];
    contactZip.keyboardType = UIKeyboardTypeDefault;
    contactZip.returnKeyType = UIReturnKeyDone;
    contactZip.clearButtonMode = UITextFieldViewModeWhileEditing;
    contactZip.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    contactZip.delegate = self;
    [self.view addSubview:contactZip];
    
//display the Select Image Button
    UIButton* selectImgBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [selectImgBtn setTitle:@"Select Image" forState:UIControlStateNormal];
    selectImgBtn.frame = CGRectMake(10, 500, self.view.frame.size.width-20, 40);
    selectImgBtn.backgroundColor = [UIColor blueColor];
    [selectImgBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selectImgBtn addTarget:self action:@selector(selectImage:) forControlEvents:UIControlEventTouchUpInside];
    [[selectImgBtn layer] setCornerRadius:8.0f];
    [[selectImgBtn layer] setBorderWidth:2.0f];
    [[selectImgBtn layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [self.view addSubview:selectImgBtn];

//display the image placeholder
    
    UIImageView *imgview = [[UIImageView alloc]
                            initWithFrame:CGRectMake((self.view.frame.size.width/2)-50 , 550, 100, 100)];
    //if the image is empty; use a placeholder image
    if(_obj == nil  || [_obj valueForKey:@"image"] == nil){

       [imgview setImage:[UIImage imageNamed:@"personIcon.png"]];
       [self.view addSubview:imgview];
    }
   else{
       [imgview setImage:[UIImage imageWithData:[_obj valueForKey:@"image"]]];
       [self.view addSubview:imgview];
   }
}

//save to core data
-(void)saveContact:(id)sender{
    
    ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [ad managedObjectContext];
    
    if(_obj == nil){  //if adding a new person
        
        //Creating a new Person object to the core data
        NSManagedObject *person = [NSEntityDescription
                                   insertNewObjectForEntityForName:@"Person"
                                   inManagedObjectContext:context];
        
        [person setValue:contactName.text forKey:@"name"];
        [person setValue:contactAddress1.text forKey:@"address_1"];
        [person setValue:contactAddress2.text forKey:@"address_2"];
        [person setValue:contactCity.text forKey:@"city"];
        [person setValue:contactState.text forKey:@"state"];
        [person setValue:contactZip.text forKey:@"zip"];
        [person setValue:contactEmail.text forKey:@"email"];
        [person setValue:contactPhone.text forKey:@"phone_number"];
        [person setValue:selectedImgData forKey:@"image"];
        
        //Saving person object
        NSError *error;
        BOOL saveSucceeded = [context save:&error];
        if (!saveSucceeded) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }else{
            NSLog(@"new contact saved");
            
            [self.navigationController popViewControllerAnimated:YES];
            
            //ViewController* root = [ViewController new];
            //[self.navigationController pushViewController:root animated:YES];

        }
        
    }else{  //if editing an existing person
        [_obj setValue:contactName.text forKey:@"name"];
        [_obj setValue:contactAddress1.text forKey:@"address_1"];
        [_obj setValue:contactAddress2.text forKey:@"address_2"];
        [_obj setValue:contactCity.text forKey:@"city"];
        [_obj setValue:contactState.text forKey:@"state"];
        [_obj setValue:contactZip.text forKey:@"zip"];
        [_obj setValue:contactEmail.text forKey:@"email"];
        [_obj setValue:contactPhone.text forKey:@"phone_number"];
        [_obj setValue:selectedImgData forKey:@"image"];
        
        //save it
        NSError *error;
        BOOL saveSucceeded = [context save:&error];
        if (!saveSucceeded) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        NSLog(@"existing contact saved");
        
        [self.navigationController popViewControllerAnimated:YES];
        //DisplayPersonViewController* dpvc = [DisplayPersonViewController new];
        //dpvc.obj = _obj;
        //ViewController* root = [ViewController new];
        //[self.navigationController pushViewController:root animated:YES];
    }
}


//Select image
-(void)selectImage:(id)sender {
   
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//the next two methods will hide the keyboard when RETURN or DONE button is touched
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}


- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    [self dismissViewControllerAnimated:picker completion:^{
        
    }];
    selectedImage = [[UIImageView alloc]initWithImage:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    selectedImage.frame = CGRectMake((self.view.frame.size.width/2)-50 , 550, 100, 100);
    [self.view addSubview:selectedImage];
    
    selectedImgData = UIImageJPEGRepresentation(selectedImage.image, 0.8);
    //Store image data in core data
    
}



@end
