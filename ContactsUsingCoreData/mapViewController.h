//
//  mapViewController.h
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/13/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AppDelegate.h"
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface mapViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate>
{
    AppDelegate* ad;
    NSManagedObject* object;
    NSArray* fetchedObjects;
    NSString* cityAddress;
    NSString* bldgAddress;
    NSString* fullAddress;
    NSString *addressURLString;
}

@property (nonatomic, retain) CLLocationManager* locationManager;
@property (nonatomic, retain)NSManagedObject* obj;

@end
