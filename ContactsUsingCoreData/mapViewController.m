//
//  mapViewController.m
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/13/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "mapViewController.h"

@interface mapViewController ()

@end

@implementation mapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//set the title in the header
    [self.navigationItem setTitle:@"Map"];

    
//get the full address
    
    //get the address1 and address2 to concatenate and put it in the bldgAddress variable
    NSString* address1_Lbl= [[NSString alloc] init];
    address1_Lbl = [_obj valueForKey:@"address_1"];
    
    NSString* address2_Lbl= [[NSString alloc] init];
    address2_Lbl = [_obj valueForKey:@"address_2"];
    
    bldgAddress = [[NSString alloc]init];
    if(address2_Lbl == nil){
        bldgAddress = address1_Lbl;
    }else{
        bldgAddress = [address1_Lbl stringByAppendingString:@", "];
        bldgAddress = [bldgAddress stringByAppendingString:address2_Lbl];
    }
    
    //get the city, state; then put it in the cityAddress variable
    NSString* cityLbl= [[NSString alloc] init];
    cityLbl = [_obj valueForKey:@"city"];
    
    NSString* stateLbl= [[NSString alloc] init];
    stateLbl = [_obj valueForKey:@"state"];
    

    cityAddress = [cityLbl stringByAppendingString:@", "];
    cityAddress = [cityAddress stringByAppendingString:stateLbl];

    //set the full address by concatenating bldgAddress and cityAddress
    fullAddress = [bldgAddress stringByAppendingString:@" "];
    fullAddress = [fullAddress stringByAppendingString:cityAddress];
    
    NSLog(@"%@",fullAddress);
    
    
//put the full address in the geocoder
    MKMapView* map = [[MKMapView alloc]initWithFrame:self.view.bounds];
    map.delegate = self;
    //    map.showsUserLocation = YES;
    [self.view addSubview:map];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:fullAddress completionHandler:^(NSArray* placemarks, NSError* error){
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
            //            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
            //            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:aPlacemark.location.coordinate];
            //[annotation setTitle:@"WSU"]; //You can set the subtitle too
            [map addAnnotation:annotation];
            
            MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
            region.center.latitude = aPlacemark.location.coordinate.latitude ;
            region.center.longitude = aPlacemark.location.coordinate.longitude;
            region.span.longitudeDelta = 0.009f;
            region.span.latitudeDelta = 0.009f;
            [map setRegion:region animated:YES];
        }
    }];
    
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // Use one or the other, not both. Depending on what you put in info.plist
    [self.locationManager requestWhenInUseAuthorization];
    //    [self.locationManager requestAlwaysAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
    
//add Go TO MAPS button
    UIButton* mapsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [mapsButton setTitle:@"Go to Maps" forState:UIControlStateNormal];
    mapsButton.frame = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 30);
    mapsButton.backgroundColor = [UIColor blueColor];
    [mapsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[mapsButton layer] setCornerRadius:8.0f];
    [[mapsButton layer] setBorderColor: [UIColor blueColor].CGColor];
    [mapsButton addTarget:self action:@selector(goToMaps:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mapsButton];
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}

//maps function
- (void)goToMaps:(id)sender{
    //NSString *addressString = @"http://maps.apple.com/?q=1+Infinite+Loop,+Cupertino,+CA";
    addressURLString = @"http://maps.apple.com/?q=";
    addressURLString = [addressURLString stringByAppendingString:fullAddress];
    
    //modify the URL String by changing the whitespace to + sign
    addressURLString = [addressURLString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSLog(@"The URL for the address is %@",addressURLString);
    
    NSURL *url = [NSURL URLWithString:addressURLString];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
