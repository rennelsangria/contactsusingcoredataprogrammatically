//
//  DisplayPersonViewController.h
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/12/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "viewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import "mapViewController.h"

@interface DisplayPersonViewController : UIViewController< UINavigationControllerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    AppDelegate* ad;
    NSManagedObject* object;
    NSArray* fetchedObjects;
    NSString* cityAddress;
    NSString* bldgAddress;
    NSString* fullAddress;
    UILabel* nameLbl;
    UILabel* phoneLbl;
    NSString* emailString;
    NSString* address1_Lbl;
    NSString* address2_Lbl;
    NSString* cityLbl;
    NSString* stateLbl;
    NSString* zipLbl;
    UIImageView *imgview;
    UIButton* emailButton;
    UILabel* addressLbl;
    UILabel* cityStateZip;
    UILabel* contactEmail;
}
@property (nonatomic, retain)NSManagedObject* obj;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
