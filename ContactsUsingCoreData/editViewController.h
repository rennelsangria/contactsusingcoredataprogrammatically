//
//  editViewController.h
//  ContactsUsingCoreData
//
//  Created by Rennel Sangria on 7/12/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DisplayPersonViewController.h"

@interface editViewController : UIViewController<UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    AppDelegate* ad;
    NSManagedObject* object;
    NSArray* fetchedObjects;
    NSString* cityAddress;
    NSString* bldgAddress;
    NSString* fullAddress;
    NSString* nameLbl;
    UITextField* contactName;
    UITextField* contactPhone;
    UITextField* contactEmail;
    UITextField* contactAddress1;
    UITextField* contactAddress2;
    UITextField* contactCity;
    UITextField* contactState;
    UITextField* contactZip;
    UIImageView* selectedImage;
    NSData* selectedImgData;
    
}

@property (nonatomic, retain)NSManagedObject* obj;

@end
